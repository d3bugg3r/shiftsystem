<?
	session_start();
	include('./includes/functions.php');
	if ($_POST['login'] == "login") { //login form submitted
		$user = escape($_POST['user']);
		$pass = md5($_POST['password']);
		$query = ("SELECT * FROM users WHERE username='$user' AND password = '$pass'");
		$result = mysql_query($query);
		if (mysql_num_rows($result) != 0) { //login successful
			$r = mysql_fetch_array($result);
			$_SESSION['logged_in'] = true;
			$_SESSION['user'] = $r[0]; //set global user ID
			$_SESSION['admin'] = $r[3]; //set admin value
			$_SESSION['username'] = $r[1];
		}
	}
	if ($_SESSION['logged_in'] != true) {
		include('login.php');
		die();
	}
	//nav_date is the date the user is currently looking at in the nav system
	if (!isset($_SESSION['nav_date'])) { //if this is not set, we will use the current date
		$_SESSION['nav_date'] = mktime(); 
	}
	$nav_date = $_SESSION['nav_date'];
	
	
	if ($_GET['p'] == "logout") {
		include ('logout.php');
		die();
	}
	$g_user = $_SESSION['user'];
?>


<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<script>
<!--
function setFocus() {
 document.form1.user.focus();
}
// --></script>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<title>Calendar System</title>
<link href='style.css' rel='stylesheet' type='text/css' />
</head>
<body>
  <div id='wraper'>
  	<div id='header_bg'>
  <div id='header'>
    <div class='logo'> <a href='#'></a> </div>
    <div class='navbar'>
    	<ul>
        	<li><a href='?p=account'>Account Settings</a></li>
            <li><a href='?p=stats'>Personal Calendar</a></li>
            <li><a href='?p=cal'>Schedule</a></li>
            <li><a href='?p=logout'>Logout</a></li>
			<?
				if ($_SESSION['admin'] == 1) {
					echo "<li><a href='?p=admin'>Admin Panel</a></li>";
				}
				if (isset($_SESSION['sched'])) {
					echo "<li><a href='?p=admin&a=cal&c=3&u=2'>Unset</a></li>";
				}
			?>
        </ul>
    </div>
  </div>
</div>
<div id='content_bg'>
	<div class='content'>
    	<div class='usertitle'>
        	<h1>Welcome</h1>
            <h2><? echo $_SESSION['username']; ?></h2>
          </div>
		  
<?
	if (!isset($_GET['p'])) $p = '';
		else $p = $_GET['p'];
	
	switch($p) {
	
		case '':
			include('./includes/calendar.php');
		break;
		
		case 'account':
			include('./includes/account.php');
		break;
		
		case 'stats':
			include('./includes/stats.php');
		break;
		
		case 'day':
			include('./includes/dayedit.php');
		break;
		
		case 'cal':
			include('./includes/calendar.php');
		break;
		
		case 'assign':
			include('./includes/assign.php');
		break;
		
		case 'remove':
			include('./includes/remove.php');
		break;
		
		case 'admin':
			include('./includes/admin.php');
		break;
	}
?>


  </div>
</body>
</html>
