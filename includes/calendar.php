<?
if (isset($_GET['date'])) {
	$new = escape($_GET['date']);
	$_SESSION['nav_date'] = strtotime($new);
}
$month = date("01-m-Y", $_SESSION['nav_date']);
$d = date("d", $_SESSION['nav_date']);
$m = date("m", $_SESSION['nav_date']);
$y = date("Y", $_SESSION['nav_date']); //these are the numerical values
$mon = date("F", $_SESSION['nav_date']);
if (isset($_SESSION['sched'])) { $c_user = $_SESSION['sched']; }
else { $c_user=$g_user; }

$dmonth = strtotime($month); //returns a timestamp for the first day of the selected month
		?>
			<div id="cal_wrapper">
			<? 
			echo "<h2>Displaying days for $mon $y</h2><br />"; 
			?>
			<table id="month">
				<tr>
					<?
					while (date("n", $dmonth) == $m) { //while the loop is in the correct month, this will print the days along the top of the table
						echo "<td>";
							echo date("j", $dmonth);
						echo "</td>";
						$dmonth = $dmonth + 86400; //adds 1 day to the datestamp
					}
					$dmonth = strtotime($month); //reset $dmonth
					?>
				</tr>
				<tr>
					<?
					//we shall now populate the second row with the correct days, seeing if there are any unassigned hours available or if the user is already scheduled to work that date
					while (date("n", $dmonth) == $m) {
						$d = date("j", $dmonth);
						$result = mysql_query("SELECT * FROM hours WHERE worker='$c_user' AND day='$d' AND month='$m' AND year='$y'"); // this will select all entries from 'hours' where 'worker' is the same as the user's ID on this day
						if (mysql_num_rows($result) != 0){ //this means the user is working this day
							echo "<td class='working'>
							<a class='cell' href='?p=cal&month=$m&year=$y&day=$d'></a>
							</td>";
						}
						else {
							$result = mysql_query("SELECT * FROM hours WHERE worker='0' AND day='$d' AND month='$m' AND year='$y'"); // this will select all entries for this day where there is no user assigned
							if (mysql_num_rows($result) != 0){
								echo "<td class='available'>
								<a class='cell' href='?p=cal&month=$m&year=$y&day=$d'></a>
								</td>";
							}
							else  //getting to this point means there are no shifts available to this user on this day
								echo "<td class='none'>";
						}						
						echo "</td>";
						$dmonth = $dmonth + 86400; //adds 1 day to the datestamp
					}
					?>
				</tr>
			</table>
			<br />
			<br />
			<table id="key">
			<tr>
				<td id="n"></td><td>No shifts Available</td><td id="w"></td><td>Shifts assigned</td><td id="a"></td><td>Shifts available</td>
			</tr>
			<!--Here the page will end until the user selects a day to view-->
			<?
			if (isset($_GET['day'])) {
				$d = escape($_GET['day']);
				$date = $d . "-" . $m . "-" . $y;
				echo "<br /><h3>Details for $date</h3>";
				echo "<table id='hours'>";
				echo "<tr>
					<td class='title'>
						Hour
					</td>
					<td class='title'>
						Available/Assigned
					</td>
					<td class='title'>
						Others Working
					</td>
				</tr>";
				//this table will display each hour in the day, showing if it is available to work or if the user is already working that hour
				$h = 0;
				while ($h <= 23) {
					$h2 = $h+1;
					if ($h<9) $h_str = "0$h:00-0$h2:00";
					elseif ($h==9) $h_str = "0$h:00-$h2:00";
					elseif (($h>9)&&($h<23)) $h_str = "$h:00-$h2:00";
					else $h_str = "23:00-00:00";
					echo "<tr>
					<td>
						$h_str
					</td>";
					$h_result = mysql_query("SELECT * FROM hours WHERE worker='$c_user' AND day='$d' AND month='$m' AND year='$y' AND hour='$h'");
					if (mysql_num_rows($h_result) == 0) { //This means that the user is not working this hour
						$hr2 = mysql_query("SELECT * FROM hours WHERE worker='0' AND day='$d' AND month='$m' AND year='$y' AND hour='$h'");
						if (mysql_num_rows($hr2) != 0) { //This means that this hour is available to work
							$h_a = mysql_fetch_array($hr2);
							$h_id = $h_a[0];
							echo "<td class='available'>";
								echo "<form name='$h_id' method='post' action='?p=assign'>
									<input type='hidden' name='c_id' value='".$c_user."' />
									<input type='hidden' name='id' value='".$h_id."' />
									<input type='hidden' name='return' value='month=$m&year=$y&day=$d' />";
									echo "<input type='submit' class='available_button' value='' />";
									echo "</form>";
								
								
						} else { //the hour is unavailable
								echo "<td class='none'>";
						}
					} else { //The user is working this hour
						$h_a = mysql_fetch_array($h_result);
						$h_id = $h_a[0];
						echo "<td class='working'>";
							echo "<form name='$h_id' method='post' action='?p=remove'>
									<input type='hidden' name='id' value='$h_id' />
									<input type='hidden' name='c_id' value='".$c_user."' />
									<input type='hidden' name='return' value='month=$m&year=$y&day=$d' />";
									
									echo "<input type='submit' class='working_button' value='' />";
									echo "</form>";
					}
					echo "</td>";
					//now we will populate the final column with the usernames of people already working that hour, if any
					echo "<td>";
					$h_result = mysql_query("SELECT * FROM hours WHERE day='$d' AND month='$m' AND year='$y' AND hour='$h' AND worker<>'$c_user' AND worker<>'0'");
					$c = 0;
					while ($h_row = mysql_fetch_row($h_result)) {
						if ($c > 0) echo ", ";
						$c++;
						$w_id = $h_row[5]; //id of the worker for this one
						$w = mysql_fetch_array(mysql_query("SELECT * FROM users WHERE ID='$w_id'"));
						echo "<span style='color:$w[5]; font-weight:bold;'>$w[1]</span>";
					}
							
				$h = $h+1;
				
				}
		
			} 
			echo "</table>";
		


// the nav bar wil display each month of the current year with options to go forwards and back a year ?>
		
<div id="footer_bg">
	<div class="footer">
    	<div align="center" class="month_nav">
			<?
			$prev_year = date("Y", strtotime("-1 year", $_SESSION['nav_date']));
			$next_year = date("Y", strtotime("+1 year", $_SESSION['nav_date']));
     		echo "<a class='nav' href='index.php?p=cal&date=01-01-$prev_year'>$prev_year &lt;&lt; -</a>"; //this displays a link to the previous year
			$i = 1;
			while ($i <=12 ){
				$str = "01-$i-$y";
				$m = date("F", strtotime($str)); //$m is the textual representation of the current month being used
				if (date("n", $_SESSION['nav_date']) == $i) {
					echo "<i> $m </i>-";
				} else {
					echo "<a class='nav' href='index.php?p=cal&date=$str'> $m -</a>";
				}		
				$i++;
			} 
			echo "<a class='nav' href='index.php?p=cal&date=01-01-$next_year'> &gt;&gt; $next_year</a>"; //this displays a link to the next year
            ?>
    </div>
    
    </div>
</div>
</div>