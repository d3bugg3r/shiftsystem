<? if ($_SESSION['admin'] == false) {
		echo "You need to be logged in as an admin to access this page";
		die();
	}
?>
<h3>Admin Control Panel</h3><br />
Please select an option to continue<br />

<a href="?p=admin&a=user">User Management</a> | <a href="?p=admin&a=cal">Shift Management</a><br />
<br />

<!--Here the page ends until an option has been selected -->
<?
	if (!isset($_GET['a'])) {
		$a = '';
	} else {
		$a = $_GET['a'];
	}
	
	switch($a) {
		case '':
			
		break;
		
		case 'user':
			include ('./includes/admin/user.php');
		break;
		
		case 'cal':
			include ('./includes/admin/cal.php');
		break;
		
		case 'set':
			include ('./includes/admin/set.php');
		break;
	}
	