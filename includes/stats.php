<?
if (isset($_GET['date'])) {
	$new = escape($_GET['date']);
	$_SESSION['nav_date'] = strtotime($new);
}
$month = date("01-m-Y", $_SESSION['nav_date']);
$d = date("d", $_SESSION['nav_date']);
$m = date("m", $_SESSION['nav_date']);
$y = date("Y", $_SESSION['nav_date']); //these are the numerical values
$mon = date("F", $_SESSION['nav_date']);

$dmonth = strtotime($month); //returns a timestamp for the first day of the selected month

echo "<br /><br /><h2>Displaying User Statistics for $mon $y</h2><br />"; 
echo "Scheduled hours for $y: " . yearly($g_user, $y). "<br />";
echo "Scheduled monthly hours:&nbsp; " . monthly($g_user, $m, $y);
?>
<h2>Export your monthly calendar to iCalendar for use in Outlook, Google Calendar, etc.</h2>
<?
	$ical="BEGIN:VCALENDAR
";
//To achieve this, we  will consolidate the assigned hours into "shifts" and create events for each
$i = 1;
set_time_limit(1);
while ($i <= 31) { //cycling through the days of the month
	$cal = mysql_query("SELECT * FROM hours WHERE worker='$g_user' AND year='$y' AND month='$m' AND day='$i' ORDER BY hour ASC"); //retrieve all the worker's hours for this day
	$c=0; 

if (mysql_num_rows($cal) != 0) {
	while ($row = mysql_fetch_row($cal)) {
		$split = $row[4]-$end;
		if (($split > 1) && ($c > 0)) { //this is a seperate shift in the same day
			if ($i < 10) $i2 = "0$i";
			else $i2 = $i;
			if ($begin < 10) $begin = "0$begin";
			if ($end < 10) $end = "0$end";
			$start = "$y$m$i2"."T$begin"."0000";
			$end = "$y$m$i2"."T$end"."0000";
			$ical_add = "BEGIN:VEVENT
DTSTART:$start
DTEND:$end
SUMMARY:Work
LOCATION:
DESCRIPTION:
PRIORITY:1
END:VEVENT
";
			
			$ical = "$ical$ical_add";
			$c=0; //reset so the next lot is a new "shift"
		} 
		if ($c==0) { //This is the first hour of the shift
			$begin = $row[4];
		}
		$end = $row[4];
		$c++;
	}

	if ($i < 10) $i2 = "0$i";
	else $i2 = $i;
	if ($begin < 10) $begin = "0$begin";
	if ($end < 10) $end = "0$end";
	$start = "$y$m$i2"."T$begin"."0000";
	$end = "$y$m$i2"."T$end"."0000";
	$ical_add = "BEGIN:VEVENT
DTSTART:$start
DTEND:$end
SUMMARY:Work
LOCATION:
DESCRIPTION:
PRIORITY:1
END:VEVENT
";
	$ical = "$ical$ical_add";
	}


	
	$i++; 
}
$ical = "$ical"."END:VCALENDAR";
$file = fopen("calendars/calendar_$m_$y_$g_user.ics", "w");
fwrite($file, $ical);
echo "Click <a href='calendars/calendar_$m_$y_$g_user.ics'>here</a> to download your calendar file";
?>	
		
<div id="footer_bg">
	<div class="footer">
    	<div align="center" class="month_nav">
			<?
			$prev_year = date("Y", strtotime("-1 year", $_SESSION['nav_date']));
			$next_year = date("Y", strtotime("+1 year", $_SESSION['nav_date']));
     		echo "<a class='nav' href='index.php?p=stats&date=01-01-$prev_year'>$prev_year &lt;&lt; -</a>"; //this displays a link to the previous year
			$i = 1;
			while ($i <=12 ){
				$str = "01-$i-$y";
				$m = date("F", strtotime($str)); //$m is the textual representation of the current month being used
				if (date("n", $_SESSION['nav_date']) == $i) {
					echo "<i> $m </i>-";
				} else {
					echo "<a class='nav' href='index.php?p=stats&date=$str'> $m -</a>";
				}		
				$i++;
			} 
			echo "<a class='nav' href='index.php?p=stats&date=01-01-$next_year'> &gt;&gt; $next_year</a>"; //this displays a link to the next year
            ?>
    </div>
    
    </div>
</div>