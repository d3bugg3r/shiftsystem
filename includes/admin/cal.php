<?
	if ($w_id = $_GET['rem_diff']) { //here we set the week back to the default structure
		mysql_query("UPDATE weeks SET standard='1' WHERE ID='$w_id'"); //set the week to standard
		mysql_query("DELETE FROM diff_weeks WHERE week_id='$w_id'"); //Remove all the entries for the custom week
	}
	
	elseif (($w_id = $_GET['add_diff']) || ($w_id=$_GET['edit_diff'])) {
	$week = mysql_query("SELECT * FROM weeks WHERE ID='$w_id'");
	$week = mysql_fetch_array($week);
	echo "<br /><h1>Editing weekly structure for week beginning $week[1]</h1>";
	echo "<h3>Choose how many workers can be scheduled for this week. A '0' value will mean no shifts available that hour</h3>";
	//now we will see if this week already has a custom structure
	$wq = mysql_query("SELECT * FROM diff_weeks WHERE week_id='$w_id'");
	if (mysql_num_rows($wq)==0) {//this table does not have a custom structure, we need to create a framework
		include ('./includes/create.php');
		mysql_query("$create");
		echo mysql_error();
	}
	//Now we must create a table allowing the admin to set which hours are available for this week
	echo "<form method='post' action='?p=admin&a=set'><table id='weekly'>
		<tr class='odd'>
			<td>
			</td>
			<td>
				Monday
			</td>
			<td>
				Tuesday
			</td>
			<td>
				Wednesday
			</td>
			<td>
				Thursday
			</td>
			<td>
				Friday
			</td>
			<td>
				Saturday
			</td>
			<td>
				Sunday
			</td>
		</tr>"; //This has created the top row of the table
	$h = 0;
	$tab=1; //used for setting the tab index
	while ($h <= 23) {
		if ($h&1) $odd = true; else $odd=false;
		$h2 = $h+1;
		if ($h<9) $h_str = "0$h:00-0$h2:00";
		elseif ($h==9) $h_str = "0$h:00-$h2:00";
		elseif (($h>9)&&($h<23)) $h_str = "$h:00-$h2:00";
		else $h_str = "23:00-00:00";
		if ($odd) {
			echo "<tr class='odd'>";
		}
		else {
			echo "<tr class='even'>";
		}
		echo "<td class='hour'>
				$h_str
			</td>";
		$d=1;
		$t=$tab;
		while ($d<8) {
			$r = mysql_query("SELECT * FROM diff_weeks WHERE week_id='$w_id' AND day='$d' AND hour='$h'");
			$r = mysql_fetch_array($r);
			echo "<td class='input'>
				<input type='text' class='hour' name='$r[0]' value='$r[4]' tabindex='$t' />
			</td>";
			$d++;
			$t=$t+24;
		}
		$h++;
		$tab++;
		echo "</tr>";
	}
	echo "<tr>
		<td colspan='8'>
			<input type='hidden' name='type' value='non' />
			<input type='hidden' name='w_id' value='$w_id' />
			<input type='submit' value='Submit Changes' />
		</td>
	</tr>";
	die();
}


?>




<h3>Shift Management</h3>
<a href="?p=admin&a=cal&c=1">Set Weekly Hours</a> | <a href="?p=admin&a=cal&c=2">Activate Weeks for Scheduling</a> | <a href="?p=admin&a=cal&c=3">Schedule Users</a>

<?

if ($_GET['c']=="1") {
	echo "<h3>Choose how many workers can be scheduled for each standard week. A '0' value will mean no shifts available that hour</h3>";
	echo "<h4>This will not affect weeks already active</h4>";
	//Now we must create a table allowing the admin to set which hours are available as standard each week
	echo "<form method='post' action='?p=admin&a=set'><table id='weekly'>
		<tr class='odd'>
			<td>
			</td>
			<td>
				Monday
			</td>
			<td>
				Tuesday
			</td>
			<td>
				Wednesday
			</td>
			<td>
				Thursday
			</td>
			<td>
				Friday
			</td>
			<td>
				Saturday
			</td>
			<td>
				Sunday
			</td>
		</tr>"; //This has created the top row of the table
	$h = 0;
	$tab=1; //used for setting the tab index
	while ($h <= 23) {
		if ($h&1) $odd = true; else $odd=false;
		$h2 = $h+1;
		if ($h<9) $h_str = "0$h:00-0$h2:00";
		elseif ($h==9) $h_str = "0$h:00-$h2:00";
		elseif (($h>9)&&($h<23)) $h_str = "$h:00-$h2:00";
		else $h_str = "23:00-00:00";
		if ($odd) {
			echo "<tr class='odd'>";
		}
		else {
			echo "<tr class='even'>";
		}
		echo "<td class='hour'>
				$h_str
			</td>";
		$d=1;
		$t=$tab;
		while ($d<8) {
			$r = mysql_query("SELECT * FROM week WHERE Day='$d' AND Hour='$h'");
			$r = mysql_fetch_array($r);
			echo "<td class='input'>
				<input type='text' class='hour' name='$r[0]' value='$r[3]' tabindex='$t' />
			</td>";
			$d++;
			$t=$t+24;
		}
		$h++;
		$tab++;
		echo "</tr>";
	}
	echo "<tr>
		<td colspan='8'>
			<input type='hidden' name='type' value='standard' />
			<input type='submit' value='Submit Changes' />
		</td>
	</tr>";
}
if ($_GET['c']=="2") {
	echo "<h4>Here you can choose weeks that can be viewed by users</h4>";
	//This will make the current and next year become available automatically for viewing
	echo "<form method='post' action='?p=admin&a=cal&c=2'>";
	echo "Select a Year to Edit: <select name='year'>";
	$current = date("Y");
	echo "<option value='$current' selected='selected'>$current</option>";
	$next = date("Y", strtotime("next year"));
	echo "<option name='$next'>$next</option>";
	echo "</select>";
	echo "<input type='submit' value='Select Year' /><br />";
	if ($y = $_POST['year']){ //the year form has been submitted now we will select the month to edit
		if (date("Y") != $y) { //A different year is selected
			$month = strtotime("01-01-$y");
		} else { $month=strtotime("last month"); } //gives a timestamp for the previous month
		echo "Select a Month to Edit: <select name='month'>";
		$i = 0;
		while (date("Y",$month)==escape($_POST['year'])) { //while the loop is in the selected year
			$i = $i+1;
			$month_num = date("n", $month);
			$month_name = date("F", $month);
			echo "<option name='$month_num'";
			if ($i==2) echo " selected='selected'";
			echo ">$month_name</option>";
			$month = strtotime("next month", $month);
		}
		echo "<input type='submit' value='Select Month' /></form>";
	}
	if ($begin = escape($_POST['wb'])) { //A week has been edited
		if (escape($_POST['active']) == "Yes") $active=1;
			else $active=0;
		$r = mysql_query("SELECT * from weeks WHERE begin='$begin'");
		if (mysql_num_rows($r)==0) { //The week doesn't exist yet, so we create it
			mysql_query("INSERT INTO weeks VALUES('','$begin', '0', '1')");
			$r = mysql_query("SELECT * from weeks WHERE begin='$begin'"); //repopulate $r so the script can continue
		}
			$r = mysql_fetch_row($r);
			$week_id = $r[0];
			$standard = $r[3];
			$prev_a = $r[2]; //the previous active setting
			if (($prev_a=="1") && ($active==0)) { //this means the week was active, but is not any longer, so we must remove all the shifts from that week
				$i=0;
				$begin = strtotime($begin);
				while ($i<=6) { //we will now cycle through the days of this week, removing all the shifts for each day
					$rem = strtotime("+$i days", $begin);
					$day = date("j", $rem); //the day value for this day
					$month = date("n", $rem);
					$year = date("Y", $rem);
					mysql_query("DELETE FROM hours WHERE day='$day' AND month='$month' AND year='$year'");
					$i++;					
				}
			}
			if (($prev_a=="0") && ($active=1)) { //this means the week is becoming active, so we need to add the shifts to the database
				$begin = strtotime($begin);
				$i = 0;
				while ($i<=6) { //cycling through the days of the week
					$add = strtotime("+$i days", $begin);
					$day = date("j", $add); //the day value for this day
					$day_num = date("N", $add);
					$month = date("n", $add);
					$year = date("Y", $add);
					if ($standard == 1) {
						$hours = mysql_query("SELECT * FROM week WHERE Day='$day_num'");
						while ($hour_row = mysql_fetch_row($hours)) { //Here we retrieve the data from the week structure and add the correct amount of shifts to each hour for the day
							$c = 1;
							while ($c<=$hour_row[3]) {
								mysql_query("INSERT INTO hours VALUES('','$year','$month','$day','$hour_row[2]','0')");
								$c++;
							}						
						}
					}
					else {
						$hours = mysql_query("SELECT * FROM diff_weeks WHERE week_id='$week_id' AND Day='$day_num'");	
						while ($hour_row = mysql_fetch_row($hours)) { //Here we retrieve the data from the week structure and add the correct amount of shifts to each hour for the day
							$c = 1;
							while ($c<=$hour_row[4]) {
								mysql_query("INSERT INTO hours VALUES('','$year','$month','$day','$hour_row[3]','0')");
								$c++;
							}						
						}
					}
					$i++;
				}
			
			}
			
			$week_id = $r[0]; //ID of the selected week
			mysql_query("UPDATE weeks SET active='$active', standard='$standard' WHERE ID='$week_id'");
			
		
		if ($standard == 0) { //the admin has chosen to have a non-standard working week
		
		}
	
	}
	if ($m = escape($_POST['month'])) { //A month has been selected for viewing
		//Now we want a list of the weeks starting in the selected month, with an option to activate/deactivate them and select a different week to the standard setup
		$monday = strtotime("01-$m-$y"); //this gives the first day of the selected month
		while (date("D", $monday) != "Mon") { //this will cycle until it finds a monday
			$monday = strtotime("+1 day", $monday);
		}
				
		echo "<table id='admin'>
			<tr>
				<td>
					Week beginning
				</td>
				<td>
					Activated
				</td>
				<td>
					Different Shift Pattern
				</td>
				<td>
				</td>
			</tr>";
			
		while (date("F", $monday) == $m) {//while we are in the selected month
			$seven = strtotime("-7 days");
			if ($monday < $seven) { //if the week began more than 7 days ago
				$disable = "disabled ='disabled'";
				
			} else {
				$disable = "/";
			}
			$wb = date("d-m-Y", $monday); //week beginning date string
			$q = mysql_query("SELECT * from weeks WHERE begin='$wb'");
			if ($r = mysql_fetch_array($q)) {
				$w_id=$r[0];
				$active = $r[2];
				$standard = $r[3];
			}
			else {
				mysql_query("INSERT INTO weeks VALUES('','$wb', '0', '1')"); //create the week we are viewing with default attributes
				$w_id = mysql_insert_id();
				$active = "0";
				$standard = "1";

			}
			echo "<form method='post' action='?p=admin&a=cal&c=2'>";
			echo "<input type='hidden' name='week_id' value'$w_id' />";
			echo "<input type='hidden' name='month' value='$m' />";
			echo "<input type='hidden' name='year' value='$y' />";
			echo "<input type='hidden' name='wb' value='$wb' />";
			echo "<tr>
				<td>
					$wb
				</td>
				<td>
					<input type='checkbox' name='active' $disable value='Yes' ";
					if ($active==1) echo "checked='yes'";
					echo " />
				</td>
				<td>";
					if ($disable == "disabled ='disabled'") {
						echo "Cannot edit weeks in the past";
					}
					elseif ($active ==1) echo "Unable to edit an active week";
					elseif ($standard == 0) {
						echo "<a href='http://rattypig.co.uk/shifts/index.php?p=admin&a=cal&c=2&edit_diff=$w_id'>Edit Weekly Hours</a>|<a href='http://rattypig.co.uk/shifts/index.php?p=admin&a=cal&c=2&rem_diff=$w_id'>Set to Default Week</a>";
					}
					else {
						echo "<a href='http://rattypig.co.uk/shifts/index.php?p=admin&a=cal&c=2&add_diff=$w_id'>Set Custom Weekly Hours</a>";
					}
				echo "</td>
				<td>
					<input type='submit' value='Edit Week' $disable/>
				</td>
			</tr>
			</form>";
			$monday = strtotime("+7 days", $monday);
			unset($disable);
		}
		
	}
}
if ($_GET['c'] == "3") {
	if ($_GET['u']=="1") {
		$u = $_POST['user'];
		$name = mysql_query("SELECT * FROM users WHERE ID='$u'");
		$name = mysql_fetch_array($name);
		$name = $name[1];
		echo "<h2>You are now able to edit the shifts of user '$name' through the main calendar section. Please user the 'Unset' link in the navigation panel to unset this</h2>";
		$_SESSION['sched'] = $u;		
	}
	else {
	if ($_GET['u'] == "2") {
		unset($_SESSION['sched']);
		echo "<br /><h1>User Unset</h1><br />";
	}	
		echo "<h2>Please select a user from the list. Once the user is selected you can edit the users shifts as if logged in as them. Please remember to unset this once you are finished</h2>";
		echo "<form action='?p=admin&a=cal&c=3&u=1' method='post'>";
		$users = mysql_query("SELECT * FROM users WHERE 1=1");
		echo "<select name='user'>";
		while ($user_row = mysql_fetch_row($users)) {
			$u = $user_row[1];
			$u_id = $user_row[0];
			echo "<option value='$u_id'>$u</option>";
		}
		echo "</select>";
		echo "<input type='submit' value='Select User' />";
	}
}