<h3>User Management</h3>

<?if ($_GET['u'] == 1) { //the new user form has been submitted
	$user = escape($_POST['user']);
	$email = escape($_POST['email']);
	$admin = escape($_POST['admin']);
	$colour = escape($_POST['colour']);
	$id = escape($_POST['id']);
	if (escape($_POST['submit']) == "Edit User"){   //Checks to see if this is an edit or a create request
		$edit = true; 
		$prev = $_POST['prev_user'];
		if ($user != $prev) { //username has been changed
			$user_val = mysql_query("SELECT * FROM users WHERE username='$user'");
			if (mysql_num_rows($user_val) != 0) { //username is not already in use
				echo "Username is already in use. Please hit back and select another";
				die();
			}
		}
		$password = $_POST['password'];
		if ($password != '') $pass = md5($password);
		if ($admin == "yes") $admin = "1"; else $admin = "0";
		if (mysql_query("UPDATE users SET username='$user', password='$pass', email='$email', admin='$admin', colour='$colour' WHERE ID='$id'")) {
			echo "User edited successfully";
			die();
		}
		else {
			echo "There was an error, please try again: ";
			echo mysql_error();
			die();
		}
		
	}
	elseif (escape($_POST['submit']) == "Delete User") { //this is a delete request
		echo "here";
		if (!isset($_POST['del_conf'])) { //deletion has not yet been confirmed
			echo "<p>Are you sure you wish to permanently delete this user? This cannot be undone</p>";
			echo "<form method='post' action='?p=admin&a=user&u=1'>";
			$id = $_POST['id'];
			echo "<input type='hidden' name='id' value='$id' />";
			echo "<input type='hidden' name='del_conf' value='true' />";
			echo "<input type='submit' name='submit' value='Delete User' />";
			echo "<input type='submit' value='Cancel' />";
			echo "</form>";			
			die();
		}
		else { //deletion has been confirmed
			$id = $_POST['id'];
			if (mysql_query("DELETE FROM users WHERE ID='$id'")) {
				echo "<p>User deleted Successfully</p>";
			}
			else echo "There was an error! ".mysql_error();
		}
	}
	elseif (escape($_POST['submit']) == "Cancel") {
	}
	else { //This is a create request
	$user_val = mysql_query("SELECT * FROM users WHERE username='$user'");
	if (mysql_num_rows($user_val) == 0) { //username is not already in use
		if ($admin=="yes") $admin = 1; else $admin=0;
		$password = generatePassword();
		$pass = md5($password);
		if (mysql_query("INSERT INTO users VALUES('', '$user', '$pass', '$admin', '$email', '$colour')")) {
			mail($email, 'User Account Created', "DO NOT REPLY TO THIS MESSAGE!
				A User account has been created for you in the Shift Management System with the following details.
				Username: $user
				Password: $password
				Please change your password when you first log in", 'From: Shift Management System');
			echo "User account created. The user has been emailed their details";
			die();
		}
		else {
			echo "There was an error creating the account. Please try again";
			die();
		}
	}
	else { 
		echo "Username is already in use. Please hit back and select another";
		die();
	}
	}
}
	
	
$users = mysql_query("SELECT * FROM users WHERE 1=1");
echo "<form method='post' action='?p=admin&a=user&u=2'>";
echo "Select a User to edit: <select name='user'>";
while ($user_row = mysql_fetch_row($users)) {
	$u = $user_row[1];
	$u_id = $user_row[0];
	echo "<option value='$u_id'>$u</option>";
}
echo "</select><input type='submit' value='Edit' /></form><br />";
if ($_GET['u'] == 2) { //The edit user form has been submitted
	$id = escape($_POST['user']);
	$result = mysql_query("SELECT * FROM users WHERE id='$id'");
	$details = mysql_fetch_array($result);
	$edit = true;
	echo "<h4>Edit User Details</h4>";
}
else echo "<h4>Create New User</h4>";

echo "<form method='post' action='?p=admin&a=user&u=1'>
<input type='hidden' name='id' value='$id' />
<table class='invisible'>
<tr>
	<td>Username:</td><td> <input name='user' value='$details[1]' type='text' /></td>
</tr>
<tr>
	<td>Email Address:</td><td> <input name='email' value='$details[4]' type='text' /></td>
</tr>";
if ($edit) {
	echo "<tr>
		<td>Password:</td><td> <input type='password' name='password' /> (Leave blank to remain the same)<input type='hidden' name='prev_user' value='$details[1]' /></td>
	</tr>";
}
else {
	echo "<tr>
		<td>Password:</td><td> Will be emailed to user</td>
	</tr>";
}
echo "<tr>
	<td>Colour Code:</td><td><input type='text' name='colour' value='$details[5]' /> <a class='tooltip'>Scroll over for More Info<span>Colour Code: Enter the colour that will represent the user.<br />Either Red, Black etc or a Hex Code (e.g. #000000)</span></a>" ;
echo "<tr>
	<td>Set as Admin:</td><td> <input type='checkbox' name='admin' value='yes'"; if (($edit) && ($details[3]==1)) echo "checked='yes'"; echo " /></td>
</tr>
</table>
Please doublecheck the details before submitting<br />";
if ($edit) {
	echo "<input type='submit' name='submit' value='Edit User' />";
	echo "<input type='submit' name='submit' value='Delete User' />";
}
else {
	echo "<input type='submit' name='submit' value='Create User' />";
}
echo "</form>";