<?
	//this will be included in the main index, making all the defined functions available, and connecting to the mySQL database

	$link=mysql_connect ('localhost', 'rota', 'letmein');	// open connection
		
	if (!$link) {
		die ("Error, we can't connect to our MySQL server.");
	}
	MySQL_Select_DB ('rota');

	function escape($x) {  //using the mysql_real_escape_string without having to contantly retype it during coding
		return mysql_real_escape_string($x);
	}
	
	function monthly($id, $m, $y) { //Takes a user ID and a month and year and returns the number of hours that user is working in the specified month
		$query = "SELECT * FROM hours WHERE worker='$id' AND month='$m' AND year='$y'";
		$result = mysql_query($query);
		return mysql_num_rows($result);
	}
	
	function yearly($id, $y) { //Takes a user ID and a year and returns the number of hours a user is working in that year
		$query = "SELECT * FROM hours WHERE worker='$id' AND year='$y'";
		$result = mysql_query($query);
		return mysql_num_rows($result);
	}
	
	function generatePassword ($length = 10)
  {

    // start with a blank password
    $password = "";

    // define possible characters - any character in this string can be
    // picked for use in the password, so if you want to put vowels back in
    // or add special characters such as exclamation marks, this is where
    // you should do it
    $possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";

    // we refer to the length of $possible a few times, so let's grab it now
    $maxlength = strlen($possible);
  
    // check for length overflow and truncate if necessary
    if ($length > $maxlength) {
      $length = $maxlength;
    }
	
    // set up a counter for how many characters are in the password so far
    $i = 0; 
    
    // add random characters to $password until $length is reached
    while ($i < $length) { 

      // pick a random character from the possible ones
      $char = substr($possible, mt_rand(0, $maxlength-1), 1);
        
      // have we already used this character in $password?
      if (!strstr($password, $char)) { 
        // no, so it's OK to add it onto the end of whatever we've already got...
        $password .= $char;
        // ... and increase the counter by one
        $i++;
      }

    }

    // done!
    return $password;

  }
	
	