-- phpMyAdmin SQL Dump
-- version 2.11.8.1deb5+lenny4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 03, 2011 at 02:43 AM
-- Server version: 5.0.51
-- PHP Version: 5.2.6-1+lenny9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `rota`
--

-- --------------------------------------------------------

--
-- Table structure for table `diff_weeks`
--

DROP TABLE IF EXISTS `diff_weeks`;
CREATE TABLE `diff_weeks` (
  `ID` int(11) NOT NULL auto_increment,
  `week_id` int(11) NOT NULL,
  `day` int(11) NOT NULL,
  `hour` int(11) NOT NULL,
  `available` int(11) NOT NULL,
  `a_id` mediumint(9) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `diff_weeks`
--


-- --------------------------------------------------------

--
-- Table structure for table `hours`
--

DROP TABLE IF EXISTS `hours`;
CREATE TABLE `hours` (
  `ID` int(11) NOT NULL auto_increment,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `day` int(11) NOT NULL,
  `hour` int(11) NOT NULL,
  `worker` int(11) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `hours`
--


-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `ID` int(11) NOT NULL auto_increment,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `admin` tinyint(1) NOT NULL default '0',
  `email` text NOT NULL,
  `colour` text NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `username`, `password`, `admin`, `email`, `colour`) VALUES
(1, 'Admin', '2ac9cb7dc02b3c0083eb70898e549b63', 1, 'jason.slowe@gmail.com', 'Red');

-- --------------------------------------------------------

--
-- Table structure for table `week`
--

DROP TABLE IF EXISTS `week`;
CREATE TABLE `week` (
  `ID` int(11) NOT NULL auto_increment,
  `Day` int(11) NOT NULL COMMENT 'Representation of days of the week, with Monday being 1, to Sunday being 7',
  `Hour` int(11) NOT NULL,
  `Available` int(11) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=169 ;

--
-- Dumping data for table `week`
--

INSERT INTO `week` (`ID`, `Day`, `Hour`, `Available`) VALUES
(1, 1, 0, 0),
(2, 1, 1, 0),
(3, 1, 2, 0),
(4, 1, 3, 0),
(5, 1, 4, 0),
(6, 1, 5, 0),
(7, 1, 6, 0),
(8, 1, 7, 0),
(9, 1, 8, 0),
(10, 1, 9, 3),
(11, 1, 10, 3),
(12, 1, 11, 3),
(13, 1, 12, 3),
(14, 1, 13, 3),
(15, 1, 14, 3),
(16, 1, 15, 3),
(17, 1, 16, 3),
(18, 1, 17, 0),
(19, 1, 18, 0),
(20, 1, 19, 0),
(21, 1, 20, 0),
(22, 1, 21, 0),
(23, 1, 22, 0),
(24, 1, 23, 0),
(25, 2, 0, 0),
(26, 2, 1, 0),
(27, 2, 2, 0),
(28, 2, 3, 0),
(29, 2, 4, 0),
(30, 2, 5, 0),
(31, 2, 6, 0),
(32, 2, 7, 0),
(33, 2, 8, 0),
(34, 2, 9, 3),
(35, 2, 10, 3),
(36, 2, 11, 3),
(37, 2, 12, 3),
(38, 2, 13, 3),
(39, 2, 14, 3),
(40, 2, 15, 3),
(41, 2, 16, 3),
(42, 2, 17, 0),
(43, 2, 18, 0),
(44, 2, 19, 0),
(45, 2, 20, 0),
(46, 2, 21, 0),
(47, 2, 22, 0),
(48, 2, 23, 0),
(49, 3, 0, 0),
(50, 3, 1, 0),
(51, 3, 2, 0),
(52, 3, 3, 0),
(53, 3, 4, 0),
(54, 3, 5, 0),
(55, 3, 6, 0),
(56, 3, 7, 0),
(57, 3, 8, 0),
(58, 3, 9, 3),
(59, 3, 10, 3),
(60, 3, 11, 3),
(61, 3, 12, 3),
(62, 3, 13, 3),
(63, 3, 14, 3),
(64, 3, 15, 3),
(65, 3, 16, 3),
(66, 3, 17, 0),
(67, 3, 18, 0),
(68, 3, 19, 0),
(69, 3, 20, 0),
(70, 3, 21, 0),
(71, 3, 22, 0),
(72, 3, 23, 0),
(73, 4, 0, 0),
(74, 4, 1, 0),
(75, 4, 2, 0),
(76, 4, 3, 0),
(77, 4, 4, 0),
(78, 4, 5, 0),
(79, 4, 6, 0),
(80, 4, 7, 0),
(81, 4, 8, 0),
(82, 4, 9, 3),
(83, 4, 10, 3),
(84, 4, 11, 3),
(85, 4, 12, 3),
(86, 4, 13, 3),
(87, 4, 14, 3),
(88, 4, 15, 3),
(89, 4, 16, 3),
(90, 4, 17, 0),
(91, 4, 18, 0),
(92, 4, 19, 0),
(93, 4, 20, 0),
(94, 4, 21, 0),
(95, 4, 22, 0),
(96, 4, 23, 0),
(97, 5, 0, 0),
(98, 5, 1, 0),
(99, 5, 2, 0),
(100, 5, 3, 0),
(101, 5, 4, 0),
(102, 5, 5, 0),
(103, 5, 6, 0),
(104, 5, 7, 0),
(105, 5, 8, 0),
(106, 5, 9, 3),
(107, 5, 10, 3),
(108, 5, 11, 3),
(109, 5, 12, 3),
(110, 5, 13, 3),
(111, 5, 14, 3),
(112, 5, 15, 3),
(113, 5, 16, 3),
(114, 5, 17, 0),
(115, 5, 18, 0),
(116, 5, 19, 0),
(117, 5, 20, 0),
(118, 5, 21, 0),
(119, 5, 22, 0),
(120, 5, 23, 0),
(121, 6, 0, 0),
(122, 6, 1, 0),
(123, 6, 2, 0),
(124, 6, 3, 0),
(125, 6, 4, 0),
(126, 6, 5, 0),
(127, 6, 6, 0),
(128, 6, 7, 0),
(129, 6, 8, 0),
(130, 6, 9, 3),
(131, 6, 10, 3),
(132, 6, 11, 3),
(133, 6, 12, 3),
(134, 6, 13, 3),
(135, 6, 14, 3),
(136, 6, 15, 3),
(137, 6, 16, 3),
(138, 6, 17, 0),
(139, 6, 18, 0),
(140, 6, 19, 0),
(141, 6, 20, 0),
(142, 6, 21, 0),
(143, 6, 22, 0),
(144, 6, 23, 0),
(145, 7, 0, 0),
(146, 7, 1, 0),
(147, 7, 2, 0),
(148, 7, 3, 0),
(149, 7, 4, 0),
(150, 7, 5, 0),
(151, 7, 6, 0),
(152, 7, 7, 0),
(153, 7, 8, 0),
(154, 7, 9, 0),
(155, 7, 10, 0),
(156, 7, 11, 0),
(157, 7, 12, 0),
(158, 7, 13, 0),
(159, 7, 14, 0),
(160, 7, 15, 0),
(161, 7, 16, 0),
(162, 7, 17, 0),
(163, 7, 18, 0),
(164, 7, 19, 0),
(165, 7, 20, 0),
(166, 7, 21, 0),
(167, 7, 22, 0),
(168, 7, 23, 0);

-- --------------------------------------------------------

--
-- Table structure for table `weeks`
--

DROP TABLE IF EXISTS `weeks`;
CREATE TABLE `weeks` (
  `ID` int(11) NOT NULL auto_increment,
  `begin` text NOT NULL COMMENT 'Week beginning (starts on a monday)',
  `active` int(11) NOT NULL default '0',
  `standard` int(11) NOT NULL default '1',
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `weeks`
--

